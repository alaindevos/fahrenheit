#!/usr//bin/python
import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class MyWindow(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title="Converter")
        self.set_size_request(400,100)

        self.box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)

        self.entry = Gtk.Entry()
        self.box.pack_start(self.entry,True,True,0)

        self.button = Gtk.Button(label="Click Here")
        self.button.connect("clicked", self.on_button_clicked)
        self.box.pack_start(self.button,True,True,0)

        self.label = Gtk.Label("_________")
        self.box.pack_start(self.label,True,True,0)

        self.add(self.box)

    def on_button_clicked(self,button):
        fs = self.entry.get_text()
        f = float(fs)
        c = (f-32)*5/9
        cs = str(round(c , 2))
        self.label.set_text(cs)

win = MyWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()
