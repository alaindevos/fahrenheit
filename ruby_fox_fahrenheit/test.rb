#!/usr/bin/ruby
require 'fox16'
include Fox

class MyMainWindow < FXMainWindow
	def initialize(app)
		super(app, "Convertor", :width => 400, :height => 200)
		entry=FXTextField.new(self,20)
		button=FXButton.new(self, "Convert")
		label=FXLabel.new(self, "_________")
		
		button.connect(SEL_COMMAND) do
			fs=entry.text
			f=fs.to_f
			c=(f-32.0)*5.0/9.0
			cs=c.to_s
			label.text=cs
		end
	end
	
	def create
		super
		show(PLACEMENT_SCREEN)
	end
end

if __FILE__ == $0
	FXApp.new do |app|
		MyMainWindow.new(app)
		app.create
		app.run
	end
end
