#!/usr/bin/python3.8
import wx

class MyFrame(wx.Frame):

	def __init__(self):
		super().__init__(parent=None, title='Convert')
		panel = wx.Panel(self)
		self.entry = wx.TextCtrl(panel, pos=(5, 5))
		button = wx.Button(panel, label='Press Me', pos=(5, 55))
		button.Bind(wx.EVT_BUTTON, self.buttonpressed)
		self.label=wx.StaticText(panel,label="______",pos=(5,105))
		self.Show()

	def buttonpressed(self, event):
		fs = self.entry.GetValue()
		f = float(fs)
		c = (f-32)*5/9
		cs = str(round(c , 2))
		self.label.SetLabel(cs)

if __name__ == '__main__':
	app = wx.App()
	frame = MyFrame()
	app.MainLoop()

