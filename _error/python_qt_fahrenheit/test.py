#/usr/bin/python3.8
import sys
from PyQt5   import QtWidgets as qtw
from PyQt5   import QtCore as qtc
from PyQt5   import QtGui as qtg

class MyMainWindow(qtw.QWidget):

	def buttonclicked(self,pressed):
		print("Hello")
		fs=self.edit.text()
		f = float(fs)
		c = (f-32)*5/9
		cs = str(round(c , 2))
		self.label.setText(cs)

	def __init__(self):
		super().__init__()
		self.mylayout=qtw.QVBoxLayout()
		self.mylayout.setAlignment(qtc.Qt.AlignTop)
		self.edit=qtw.QLineEdit()
		self.mylayout.addWidget(self.edit)
		button=qtw.QPushButton("Convert")
		button.clicked.connect(self.buttonclicked)
		self.mylayout.addWidget(button)
		self.label=qtw.QLabel("-------")
		self.mylayout.addWidget(self.label)
		self.setLayout(self.mylayout)
		self.setWindowTitle("Convert")
		self.resize(400,300)

def doit():
	app=qtw.QApplication(sys.argv)
	window=MyMainWindow()
	window.show()
	myexitcode=app.exec_() #because exec is a keyword
	sys.exit(myexitcode)


if __name__ == '__main__':
	doit()

