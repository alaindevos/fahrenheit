using Gtk;
using System;
 
class SharpApp : Window {

	Entry entry;
	Button button;
    Label label;

    void buttonclicked(object sender,EventArgs args){
		string fs=entry.Text;
		float f=float.Parse(fs);
		float c=(f-32.0f)*5.0f/9.0f;
        string cs=Convert.ToString(c);
		label.Text=cs;
	}
    
    public SharpApp() : base("Entry")
    {
        SetDefaultSize(250, 200);
        SetPosition(WindowPosition.Center);
        BorderWidth = 7;
        DeleteEvent += delegate { Application.Quit(); };
        
        entry = new Entry("");

		button = new Button("Convert");
		button.Clicked+=buttonclicked;

        label = new Label("_________");


        Fixed fix = new Fixed();
        fix.Put(entry,  60,  40);
        fix.Put(button, 60, 100);
        fix.Put(label,  60, 160);

        Add(fix);

        ShowAll();
    }

    public static void Main(){
        Application.Init();
        new SharpApp();
        Application.Run();
    }
}
