package main

import (
	"strconv"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/data/binding"
	"fyne.io/fyne/v2/widget"
)

func makeUI() fyne.CanvasObject {
	estring := ""
	myentrystring := binding.BindString(&estring)
	mylabelstring := binding.NewString()
	myentry := widget.NewEntryWithData(myentrystring)
	mylabel := widget.NewLabelWithData(mylabelstring)
	mybutton := widget.NewButton("Convert",
		func() {
			myinput, _ := strconv.Atoi(estring)
			myinput = myinput * 3
			myoutput := strconv.Itoa(myinput)
			mylabelstring.Set(myoutput)
		})
	return container.NewVBox(myentry, mybutton, mylabel)
}

func main() {
	a := app.New()
	w := a.NewWindow("Widget Binding")

	w.SetContent(makeUI())
	w.ShowAndRun()
}
