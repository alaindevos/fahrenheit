#!/usr/bin/python3.8
import tkinter

def buttonpressed():
    fahrenheit = float(entry.get())
    celsius = (fahrenheit-32)*5/9
    label.config(text=round(celsius, 2))

window = tkinter.Tk()
window.title("C to F Converter")
window.minsize(width=250, height=50)

entry = tkinter.Entry(justify=tkinter.RIGHT)
label = tkinter.Label(text="", justify=tkinter.RIGHT, width=8)
button = tkinter.Button(text='Convert', command=buttonpressed)

entry.pack(padx=20, pady=20)
label.pack(padx=20, pady=20, side=tkinter.RIGHT)
button.pack(padx=20, pady=20)

window.mainloop()
