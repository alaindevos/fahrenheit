import std.stdio;
import std.conv;
import gtk.Main;
import gtk.MainWindow;
import gtk.Box;
import gtk.Widget;
import gtk.Entry;
import gtk.Button;
import gtk.Label;

class MyWindow : MainWindow{
	
	Entry entry;
	Button button;
	Label label;
	Box box;
	
	void buttonpressed(){
		string fs=entry.getText();
		float f=to!float(fs);
		float c=(f-32.0)*5.0/9.0;
        string cs=to!string(c);
		label.setText(cs);
	}

	this(){
		super("My Window");
		setSizeRequest(300,200);
		addOnDestroy(delegate void(Widget w) { quit(); } );
		box=new Box(Orientation.VERTICAL,5);
		
		entry=new Entry();
		box.packStart(entry,0,0,0);
		
		button=new Button("Convert");
		box.packStart(button,0,0,0);

		label=new Label("____");
		box.packStart(label,0,0,0);

		button.addOnClicked(delegate void(Button b){buttonpressed();});

		add(box);
		showAll();
	}

	void quit(){
		Main.quit();
	}
}

int main(string[] args){
	Main.init(args);
	MyWindow window= new MyWindow();
	Main.run();
	return 0;
}
